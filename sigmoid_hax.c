// Created by MICHAEL McDERMOTT on 5/4/16.
// License: MIT Licence https://opensource.org/licenses/MIT


#include <stdint.h>
#include <math.h>

int fastsig_i32(int x) {
    /*  Using integers as fixed-point fractions, this function uses a Taylor series to approximate a sigmoid function
     * e.g. 1/(1 + exp(-x)).
     * The cubed term can't exceed 2^31, so that gives an upper bound of 2^10 = 1024
     */
    if (x > 1024)  return 1024;
    if (x < -1024)  return -1024;
    return ( (3*x) - ((x*x*x)>>20) ) >> 1;
}

int16_t fastsig_i16(int16_t x) {
    /*  Using integers as fixed-point fractions, this function uses a Taylor series to approximate a sigmoid function
        The cubed term can't exceed 2^15, so that gives an upper bound of 2^5 = 32
        You'd think that would be super janky given the 6 bit depth output, but it is suprisingly smooth!
        See: https://imgur.com/3N7ysbb
     */
    if (x > 32) return 32;
    if (x < -32) return -32;
    return ((96 * x) - ((x * x * x) >> 5)) >> 6;
}

int16_t fastsig_shifted_16(int16_t x) {
    x -= 32;
    if (x > 32) return 32;
    if (x < -32) return -32;
    return (((96 * x) - ((x * x * x) >> 5)) >> 6);
}



int16_t pseudocos_neg(int16_t x) {
    // Returns roughly -cos(Ax), where A = 2pi/128
    if (x < 0)      x = 128-x;
    if (x > 128)    x %= 128;
    if (x > 64 && x <= 128)  x = 128-x;
    return fastsig_shifted_16(x);
}


double sigmoid(double x) {
    return 1.0 / (1.0 + exp(-x));
}


