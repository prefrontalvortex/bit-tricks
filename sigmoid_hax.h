// Created by MICHAEL McDERMOTT on 5/4/16.
// mmcdermott2@albany.edu   mikemcdermott23@gmail.com

#ifndef MISC_SIGMOID_HAX_H
#define MISC_SIGMOID_HAX_H

#include <stdint.h>
#include <math.h>

int fastsig_i32(int x);

int16_t fastsig_i16(int16_t x);

double sigmoid(double x);

#endif //MISC_SIGMOID_HAX_H
